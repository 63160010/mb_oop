import 'dart:io';
double sum=0;


mixin Square{
  double square(width) {
    sum=width*width;
    return sum;
  }
}
mixin Rectangle{
  double rectangle(width,height) {
    sum=width*height;
    return sum;
  }
}
mixin Triangle{
  double triangle(width,height) {
    double half=0.5;
    sum=width*height*half;
    return sum;
  }
}
mixin Circle{
  double circle(double radius) {
    double pieR=22/7;
    sum=radius*radius*pieR;
    return sum;
    
  }
}

double height=0;
double width=0;
double radius=0;
 class Shape with Circle,Triangle,Rectangle,Square {
  void shap(){
    circle(radius);
    triangle(width,height);
    rectangle(width,height);
    square(width);
  }
}

class SCircle extends Shape{
  @override
  circle(radius);
}
class SSquare extends Shape{
  @override
  square(width);
}
class SRectangle extends Shape{
  @override
  rectangle(width,height);
  
}
class STriangle extends Shape{
  @override
  triangle(width,height);
}

void main() {
  SCircle sc=SCircle();
  SSquare ss=SSquare();
  SRectangle sr=SRectangle();
  STriangle st=STriangle();

  print("What shape do you want to find the area of?");
  print("1 Circle");
  print("2 Rectangle");
  print("3 Square");
  print("4 Triangle");
  int shap = int.parse(stdin.readLineSync()!);


  if(shap==1){
    
    print('Circle');
    print('input radius');
    double radius = double.parse(stdin.readLineSync()!);
    sc.circle(radius);
    print(sum);
    
  }
  else if(shap==2){
    
    print('Rectangle');
    print('input width');
    double width = double.parse(stdin.readLineSync()!);
    print('input height');
    double height = double.parse(stdin.readLineSync()!);
    sr.rectangle(width,height);
    print(sum);
  }
  else if(shap==3){
    
    print('Square');
    print('input width');
    double width = double.parse(stdin.readLineSync()!);
    ss.square(width);
    print(sum);
  }
  else if(shap==4){
    
    print('Triangle');
    print('input width');
    double width = double.parse(stdin.readLineSync()!);
    print('input height');
    double height = double.parse(stdin.readLineSync()!);
    st.triangle(width,height);
    print(sum);
  }

}