import 'dart:io';

mixin Swim{
  void swim(){
    print('Swimming');
  }
}
mixin Bite{
  void bite() {
    print('Chomp');
  }
}
mixin Crawl{
  void crawl() {
    print('Crawling');
  }
}

abstract class Reptile with Swim,Bite,Crawl{
  void hunt() {
    print('Alligator -------');
    swim();
    crawl();
    bite();
    print('Eat Fish');
  }
}

 class Alligator extends Reptile {}
class Crocodile extends Reptile{

  void hunt() {
    print('Crocodile -------');
    swim();
    crawl();
    bite();
    print('Eat Zebra');
  }
}

class Fish with Swim,Bite{
  void hunt() {
    print('Crocodile -------');
    swim();
    bite();
    print('Eat Zebra');
  }
}
void main() {
  Alligator all1=Alligator();
  Crocodile cro=Crocodile();
  all1.hunt();
  cro.hunt();
}
