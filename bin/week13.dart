import 'package:week13/week13.dart' as week13;
import 'dart:io';

class Interface1 {
  void display1() {
    print(" This is the function of the interface class ' Interface1 '. ");
  }
}

class Interface2 {
  void display2() {
    print(" This is the function of the interface class ' Interface2 '. ");
  }
}

class Interface3 {
  void display3() {
    print(" This is the function of the interface class ' Interface3 '. ");
  }
}



class Subclass implements Interface1, Interface2, Interface3 {
  @override
  void display1() {
    print(" Overriding the display1( ) function in the subclass. ");
  }

  @override
  void display2() {
    print(" Overriding the display2( ) function in the subclass. ");
  }

  @override
  void display3() {
    print(" Overriding the display3( ) function in the subclass. ");
  }
}

void main() {
  Subclass s1 = Subclass();
  s1.display1();
  s1.display2();
  s1.display3();
}
